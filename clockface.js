function Clockface(_name){
    // _name: String
    var el,
        name,
        cur_offset;

    function setYOffset_callback(offset)
    {//{{{
        return function(){
            setYOffset(offset);
        }
    }//}}}

    function setYOffset_animated(offset, _numframes)
    {//{{{
        var numframes = _numframes || 1,
            step = (offset - cur_offset) / numframes,
            i;

        for(i = 0; i <= numframes; i++){
            setTimeout(setYOffset_callback(cur_offset + (step * i)), (1000 / 60) * i)
        }

        cur_offset = offset;
    }//}}}

    function setYOffset(offset)
    {//{{{
        var offset_str = "0 %ipx"
            .replace("%i", offset);
        
        el.style.backgroundPosition = offset_str;
    }//}}}

    function appendHTML(clockface)
    {//{{{
        clockface.attachHTML(el);
    }//}}}

    function attachHTML(element)
    {//{{{
        element.appendChild(el);
    }//}}}

    function Clockface()
    {//{{{
        el = document.createElement("div");
        name = _name;
        cur_offset = 0;
        el.className = "clockface " + name;

        return {
            appendHTML: appendHTML,
            attachHTML: attachHTML,
            setYOffset: setYOffset_animated,
        };
    }//}}}

    return Clockface();
}
