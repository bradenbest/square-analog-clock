var util = (function(){
    function getarg(object, key)
    {//{{{
        var value = (object || new Object())[key];

        if(typeof value === "undefined")
            return null;

        return value;
    }//}}}

    function obj_to_array(object)
    {//{{{
        var array = new Array();

        Object.keys(object).forEach(function(el){
            array.push(object[el]);
        });

        return array;
    }//}}}

    function obj_merge(object, object_src)
    {//{{{
        var keys = Object.keys(object_src);

        keys.forEach(function(k){
            object[k] = object_src[k];
        });
    }//}}}

    function html_chain(array, container)
    {//{{{
        var i;

        for(i = 0; i < array.length - 1; i++){
            array[i + 1].appendHTML(array[i]);
        }

        array[i].attachHTML(container);
    }//}}}

    function try_vertical_center()
    {//{{{
            var bigcontainer = document.querySelector(".bigcontainer"),
                pageHeight = window.innerHeight,
                centerY =  (pageHeight / 2) - (300 / 2),
                margin = "%ipx 0 0 0".replace("%i", centerY);

            bigcontainer.style.margin = margin;
    }//}}}

    function namespace_util()
    {//{{{
        return {
            getarg: getarg,
            obj_to_array: obj_to_array,
            obj_merge: obj_merge,
            html_chain: html_chain,
            try_vertical_center: try_vertical_center
        };
    }//}}}

    return namespace_util();
})();
