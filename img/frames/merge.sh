#!/bin/bash
# Requires imagemagick

png(){
    for arg in $@; do
        echo $arg.png
    done
}

mk_sprite_sheet(){
    dir=$1
    s=$2
    e=$3
    filename=$(echo $dir | tr '/' '-').png
    here=$(pwd)

    cd $dir/
    convert $(png $(seq $s $e)) -append $filename
    echo "Created sprite sheet $filename"
    mv $filename $here/ -i
    exit 0
}

parse_hours(){
    prefix=$1
    suffix=$2

    streq "$prefix" "digital" && mk_sprite_sheet "$prefix/hours/$suffix" 0 9
    streq "$prefix" "analog"  && mk_sprite_sheet "$prefix/hours" 0 23
}

parse_minutes(){
    prefix=$1
    suffix=$2

    streq "$prefix" "digital" && mk_sprite_sheet "$prefix/minutes/$suffix" 0 9

    if streq "$prefix" "analog"; then
        streq "$suffix" "extra"     && mk_sprite_sheet "$prefix/minutes/$suffix" 0 119
        streq "$suffix" "frames"    && mk_sprite_sheet "$prefix/minutes" 1 60
        streq "$suffix" "alternate" && mk_sprite_sheet "$prefix/minutes/$suffix" 0 59
    fi
}

parse_seconds(){
    prefix=$1
    suffix=$2

    streq "$prefix" "digital" && mk_sprite_sheet "$prefix/seconds/$suffix" 0 9

    if streq "$prefix" "analog"; then
        streq "$suffix" "extra"     && mk_sprite_sheet "$prefix/seconds/$suffix" 0 119
        streq "$suffix" "frames"    && mk_sprite_sheet "$prefix/seconds" 1 60
        streq "$suffix" "alternate" && mk_sprite_sheet "$prefix/seconds/$suffix" 0 59
    fi
}

usage(){
    echo "Usage: ./merge.sh [options]"
    echo "Options:"
    echo "    prefixes:"
    echo "      -d  digital"
    echo "      -a  analog"
    echo "    suffixes:"
    echo "        digital:"
    echo "          -o  ones digits"
    echo "          -t  tens digits"
    echo "        analog:"
    echo "          -e  extra-length spritesheet (minutes/seconds only)"
    echo "          -f  frames (normal spritesheet)"
    echo "          -l  alternate spritesheet (minutes/seconds only)"
    echo "    types:"
    echo "      -h  hours"
    echo "      -m  minutes"
    echo "      -s  seconds"
    echo
    echo "One of each category (prefix, suffix, type) must be selected"
    echo "Example: ./merge.sh -d -h -o -- renders digital/hours/ones"
    exit 1
}

strex(){
    [[ $1 == "." ]] && return 1

    return 0
}

sanity_check(){
    for arg in $@; do
        strex "$arg" || return 1
    done
    return 0
}

streq(){
    str1=$1
    str2=$2

    [[ $str1 == $str2 ]] && return 0 || return 1
}

parse_args(){
    prefix= #d,a
    dir= #h,m,s
    suffix= #o,t,e,f,l
    # range shall be decided based on prefix and suffix.
    [[ $# -eq 0 ]] && usage

    for arg in $@; do
        streq "$arg" "-d" && prefix="digital"
        streq "$arg" "-a" && prefix="analog"

        streq "$arg" "-o" && suffix="ones"
        streq "$arg" "-t" && suffix="tens"
        streq "$arg" "-e" && suffix="extra"
        streq "$arg" "-f" && suffix="frames"
        streq "$arg" "-l" && suffix="alternate"

        streq "$arg" "-h" && dir="hours"
        streq "$arg" "-m" && dir="minutes"
        streq "$arg" "-s" && dir="seconds"
    done

    sanity_check "${dir}." "${prefix}." "${suffix}." || usage

    streq "$dir" "hours"   && parse_hours   $prefix $suffix
    streq "$dir" "minutes" && parse_minutes $prefix $suffix
    streq "$dir" "seconds" && parse_seconds $prefix $suffix
}

parse_args $@
