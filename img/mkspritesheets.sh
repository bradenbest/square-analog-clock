#!/bin/bash

cd frames

echo "Generating Digital sprite sheets..."
./merge.sh -d -h -o
./merge.sh -d -h -t
./merge.sh -d -m -o
./merge.sh -d -m -t
./merge.sh -d -s -o
./merge.sh -d -s -t

echo "Generating Analog sprite sheets..."
./merge.sh -a -h -f
./merge.sh -a -m -f
./merge.sh -a -s -f

echo "Moving spritesheets to spritesheets/"
mv *.png ../spritesheets
