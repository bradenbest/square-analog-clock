function Display(args){
    /* HTMLElement container
     * Boolean show_digits
     * Number anim_steps
     */
    var container,
        faces,
        show_digits,
        anim_steps;

    function update_hour_digits(hours)
    {//{{{
        var hours_ones = hours % 10,
            hours_tens = parseInt((hours - hours_ones) / 10);

        if(!show_digits)
            return;

        faces.hour_d_tens.setYOffset(hours_tens * -300, anim_steps);
        faces.hour_d_ones.setYOffset(hours_ones * -300, anim_steps);
    }//}}}

    function update_minute_digits(minutes)
    {//{{{
        var minutes_ones = minutes % 10,
            minutes_tens = parseInt((minutes - minutes_ones) / 10);

        if(!show_digits)
            return;

        faces.minute_d_tens.setYOffset(minutes_tens * -300, anim_steps);
        faces.minute_d_ones.setYOffset(minutes_ones * -300, anim_steps);
    }//}}}

    function update_second_digits(seconds)
    {//{{{
        var seconds_ones = seconds % 10,
            seconds_tens = parseInt((seconds - seconds_ones) / 10);

        if(!show_digits)
            return;

        faces.second_d_tens.setYOffset(seconds_tens * -300, anim_steps);
        faces.second_d_ones.setYOffset(seconds_ones * -300, anim_steps);
    }//}}}

    function update_hour(hours)
    {//{{{
        faces.hour.setYOffset(hours * -300);
        update_hour_digits(hours);
    }//}}}

    function update_minute(minutes)
    {//{{{
        faces.minute.setYOffset(minutes * -300);
        update_minute_digits(minutes);
    }//}}}

    function update_second(seconds)
    {//{{{
        faces.second.setYOffset(seconds * -300);
        update_second_digits(seconds);
    }//}}}

    function update(time)
    {//{{{
        update_hour(time.h);
        update_minute(time.m);
        update_second(time.s);
        util.try_vertical_center();
    }//}}}

    function Display()
    {//{{{
        show_digits = util.getarg(args, "show_digits");
        anim_steps = util.getarg(args, "anim_steps") || 1;
        container = util.getarg(args, "container") || document.body;
        faces = {
            second:        new Clockface("second"),
            minute:        new Clockface("minute"),
            hour:          new Clockface("hour"),
            frame:         new Clockface("frame")
        };

        if(show_digits){
            util.obj_merge(faces, {
                second_d_ones: new Clockface("second_d_ones"),
                second_d_tens: new Clockface("second_d_tens"),
                minute_d_ones: new Clockface("minute_d_ones"),
                minute_d_tens: new Clockface("minute_d_tens"),
                hour_d_ones:   new Clockface("hour_d_ones"),
                hour_d_tens:   new Clockface("hour_d_tens")
            });
        }

        util.html_chain(util.obj_to_array(faces), container);

        return {
            update: update,
        };
    }//}}}

    return Display();
}
