## Square Analog Clock

This is a 24-hour square analog clock. 90% of the development time was spent designing the frames/"sprites" in GIMP. Which took me probably 6-8 hours total. You can see the resources in `img/frames/`

...What? I'm not a graphic designer. This stuff takes me a while :V

## Configuration

Upon opening index.html, you'll notice this:


    var clock = new Clock({
        ...
    });

This is where you can configure the clock. The options are documented in their respective files.

    function Clock(args){
    /* Display display
     * Number  fps
     */

    function Display(args){
    /* HTMLElement container
     * Boolean show_digits
     * Number anim_steps
     */

## Here's what it looks like:

![Image](https://gitlab.com/bradenbest/square-analog-clock/raw/master/img/example.png)
