function Clock(args){
    /* Display display
     * Number  fps
     */
    var display,
        interval,
        rate,
        dummy_time;

    function query()
    {//{{{
        var date = new Date();

        return {
            h: date.getHours(),
            m: date.getMinutes(),
            s: date.getSeconds()
        };
    }//}}}

    function run(unused)
    {//{{{
        var time = query();

        display.update(time);
    }//}}}

    function start()
    {//{{{
        interval = setInterval(run, rate);
    }//}}}

    function tick()
    {//{{{
        window.requestAnimationFrame(run);
    }//}}}

    function start_experimental()
    {//{{{
        interval = setInterval(tick, rate);
    }//}}}

    function stop()
    {//{{{
        clearInterval(interval);
    }//}}}

    function set_dummy_time(new_dummy_time)
    {//{{{
        dummy_time = new_dummy_time;
    }//}}}

    function increment_dummy_time()
    {//{{{
        dummy_time.s++;

        if(dummy_time.s == 60)
            dummy_time.m++;

        if(dummy_time.m == 60)
            dummy_time.h++;

        dummy_time.s %= 60;
        dummy_time.m %= 60;
        dummy_time.h %= 24;

    }//}}}

    function run_test(unused)
    {//{{{
        increment_dummy_time();

        display.update(dummy_time);
    }//}}}

    function tick_test()
    {//{{{
        window.requestAnimationFrame(run_test);
    }//}}}

    function test()
    {//{{{
        interval = setInterval(tick_test, 1000 / 60);
    }//}}}

    function Clock()
    {//{{{
        display = util.getarg(args, "display") || new Display();
        rate = 1000 / (util.getarg(args, "fps") || 1);
        dummy_time = { h: 0, m: 0, s: 0 };

        return {
            test: test,
            set_dummy_time: set_dummy_time,
            start: start,
            start_experimental: start_experimental,
            stop: stop,
        };
    }//}}}

    return Clock();
}
